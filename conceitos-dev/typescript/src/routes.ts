import { Request, Response } from "express";

import createUser from "./services/CreateUser";

export function helloWorld(request: Request, response: Response) {
  const user = createUser({
    email: "jordan@gmail.com",
    password: "12345678",
    techs: [
      "Node",
      "React",
      "React Native",
      {
        title: "JavaScript",
        experience: 100,
      },
    ],
  });

  return response.json({ message: "Hello World" });
}
"";
