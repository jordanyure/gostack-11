import React, {useState, useEffect} from "react";
import api from './services/api';
import './App.css';

import Header from "./components/Header";

/**
 * Componente
 * Propriedade
 * Estado e Imutabilidade
 */
function App() {
  
  const [projects, setProjects] = useState([]);

  useEffect(()=> {
     api.get(`/projects`).then(response => {
      setProjects(response.data)
     }) 
  }, []);

  //useState retorna um array com 2 posicoes
  //
  // 1 posicao do array . Variavel com seu valor inicial
  // 2. funcao para atualizarmos esse valor


  async function handleAddProject() {
    // projects.push(`Novo projeto ${Date.now()}`)
    // setProjects([...projects, `Novo projeto ${Date.now()}`]);
    const response = await api.post('projects', {
      title: `Front End ${Date.now()}`,
      owner: "alo"
    })  ;
    const project = response.data;

    setProjects([...projects, project])
  }

  return (
    <>
      
      <Header title="Projects" />
        <ul>
          {projects.map(project => <li key={project.id}>{project.title}</li>)}
        </ul>
        <button type="button" onClick={handleAddProject}>Adicionar projeto</button>
    </>
  );
}

export default App;
